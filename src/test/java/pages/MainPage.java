package pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import static com.codeborne.selenide.Selenide.$x;

public class MainPage {

    private static final String BASE_URL = "https://www.avito.ru/";
    private final SelenideElement carCategory = $x("//select[@id='category']/option[text()='Автомобили']");

    public MainPage open() {
        //обход блокировки авито: открыть авито, затем яндекс, затем снова авито, именно в такой последовательности
        Selenide.open(BASE_URL);
        Selenide.open("https://yandex.ru/");
        Selenide.open(BASE_URL);
        return this;
    }

    public MainPage atPage()  {
        carCategory.shouldBe(Condition.visible);
        return this;
    }

    public SearchPage clickCarCategory() {
        carCategory.click();
        return new SearchPage();
    }

}
