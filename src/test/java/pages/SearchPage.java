package pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import java.util.ArrayList;
import java.util.List;

import static com.codeborne.selenide.Selenide.*;

public class SearchPage {

    private final SelenideElement inputAutoBrand = $x("//input[@placeholder='Марка']");
    private final SelenideElement priceFrom = $x("//input[@data-marker='price/from']");
    private final SelenideElement priceTo = $x("//input[@data-marker='price/to']");
    private final SelenideElement sProbegomButton = $x("//span[text()='С пробегом']/..");
    private final SelenideElement yearFrom = $x("//input[@placeholder='от 1960']");
    private final SelenideElement yearTo = $x("//input[@placeholder='до 2022']");
    private final SelenideElement inputGearbox = $x("//input[@placeholder='Коробка передач']");
    private final SelenideElement allTypeAutoGearbox = $x("//label[text()='Автоматическая']");
    private final SelenideElement bodyTypeCoupe = $x("//i[contains(@class, 'coupe')]/..");
    private final SelenideElement bodyTypeCabriolet = $x("//i[contains(@class, 'cabriolet')]/..");
    private final SelenideElement gasolineFuelCheckBox = $x("//span[text()='бензин']");
    private final SelenideElement engineCapacityFrom = $x("//input[@placeholder='от 0,0']");
    private final SelenideElement engineCapacityTo = $x("//input[@placeholder='до 6,0+']");
    private final SelenideElement engineCapacityTitle = $x("//span[text()='Объём двигателя, л']");
    private final SelenideElement frontWheelDrive = $x("//li[@title='передний']//input[@type='checkbox']/..");
    private final SelenideElement rightWheel = $x("//span[text()='Правый']/..");
    private final SelenideElement russianRegistration = $x("//span[text()='Есть']/..");
    private final SelenideElement notCrushed = $x("//span[text()='Кроме битых']/..");
    private final SelenideElement horsePowerFrom = $x("//input[@data-marker='params[1286]/from/input']");
    private final SelenideElement horsePowerTo= $x("//input[@data-marker='params[1286]/to/input']");
    private final SelenideElement horsePowerTitle= $x("//span[text()='Мощность, л.с.']");
    private final SelenideElement showResultsButton= $x("//button[@data-marker='search-filters/submit-button']");
    private final ElementsCollection titleResultElements = $$x("//h3[@itemprop='name']");
    private final ElementsCollection priceResultElements = $$x("//meta[@itemprop='price']");

    public SearchPage atPage()  {
        inputAutoBrand.shouldBe(Condition.visible);
        return this;

    }

    public SearchPage setBrand(String brand) {
        inputAutoBrand.scrollTo().click();
        inputGearbox.sendKeys(brand);
        $x("//span[text()='" + brand + "']").click();
        return new SearchPage();
    }

    public SearchPage setPriceFrom(String price) {
        priceFrom.scrollTo().sendKeys(price);
        return new SearchPage();
    }

    public SearchPage setPriceTo(String price) {
        priceTo.scrollTo().sendKeys(price);
        return new SearchPage();
    }

    public SearchPage setSProbegomOption() {
        sProbegomButton.scrollTo().click();
        return new SearchPage();
    }

    public SearchPage setYearFrom(String year) {
        yearFrom.scrollTo().sendKeys(year);
        return new SearchPage();
    }

    public SearchPage setYearTo(String year) {
        yearTo.scrollTo().sendKeys(year);
        return new SearchPage();
    }

    public SearchPage setAllTypeAutoGearBox() {
        inputGearbox.scrollTo().click();
        allTypeAutoGearbox.click();
        inputGearbox.click();
        return new SearchPage();
    }

    public SearchPage setBodyTypeCoupe() {
        bodyTypeCoupe.scrollTo().click();
        return new SearchPage();
    }

    public SearchPage setBodyTypeCabriolet() {
        bodyTypeCabriolet.scrollTo().click();
        return new SearchPage();
    }

    public SearchPage setGasolineFuel() {
        gasolineFuelCheckBox.scrollTo().click();
        return new SearchPage();
    }

    public SearchPage setEngineCapacityFrom(String litre) {
        engineCapacityFrom.scrollTo().sendKeys(litre);
        engineCapacityTitle.click();
        return new SearchPage();
    }

    public SearchPage setEngineCapacityTo(String litre) {
        engineCapacityTo.scrollTo().sendKeys(litre);
        engineCapacityTitle.click();
        return new SearchPage();
    }

    public SearchPage setFrontWheelDrive() {
        frontWheelDrive.scrollTo().click();
        engineCapacityTitle.click();
        return new SearchPage();
    }

    public SearchPage setRightWheel() {
        rightWheel.scrollTo().click();
        return new SearchPage();
    }

    public SearchPage setRussianRegistration() {
        russianRegistration.scrollTo().click();
        return new SearchPage();
    }

    public SearchPage setNotCrushed() {
        notCrushed.scrollTo().click();
        return new SearchPage();
    }

    public SearchPage setHorsePowerFrom(String horsePower) {
        horsePowerFrom.scrollTo().sendKeys(horsePower);
        horsePowerTitle.click();
        return new SearchPage();
    }

    public SearchPage setHorsePowerTo(String horsePower) {
        horsePowerTo.scrollTo().sendKeys(horsePower);
        horsePowerTitle.click();
        return new SearchPage();
    }

    public SearchPage clickShowResults() {
        showResultsButton.scrollTo().click();
        return new SearchPage();
    }

    public List<String> getBrandResults() {
        List<String> brands = new ArrayList<>();
        for (SelenideElement element : titleResultElements) {
            String[] words = element.getText().split(" ");
            brands.add(words[0]);
        }
        return brands;
    }

    public List<Integer> getYearResults() {
        List<Integer> years = new ArrayList<>();
        for (SelenideElement element : titleResultElements) {
            String[] words = element.getText().split(" ");
            years.add(Integer.parseInt(words[words.length - 1]));
        }
        return years;
    }

    public List<Integer> getPriceResults() {
        List<Integer> prices = new ArrayList<>();
        for (SelenideElement element : priceResultElements) {
            prices.add(Integer.parseInt(element.getAttribute("content")));
        }
        return prices;
    }

}
