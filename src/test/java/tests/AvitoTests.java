package tests;

import io.qameta.allure.Description;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import pages.MainPage;
import pages.SearchPage;

@DisplayName("Авито тесты")
public class AvitoTests extends BaseTest {

    private final static String BRAND = "Honda";
    private final static Integer PRICE_FROM = 100000;
    private final static Integer PRICE_TO = 500000;
    private final static Integer YEAR_FROM = 2000;
    private final static Integer YEAR_TO = 2010;

    @DisplayName("Тест фильтров поиска объявлений")
    @Test
    @Description("Выбирается категория автомобили, далее выставляются фильтры, " +
            "выполняется поиск объявлений, после идет сверка выставленных фильтров и полученных объявлений")
    public void filterTest() {
        MainPage mainPage = new MainPage();
        SearchPage searchPage = new SearchPage();

        mainPage
                .open()
                .atPage()
                .clickCarCategory()
                .atPage()
                .setBrand(BRAND)
                .setPriceFrom(PRICE_FROM.toString())
                .setPriceTo(PRICE_TO.toString())
                .setYearFrom(YEAR_FROM.toString())
                .setYearTo(YEAR_TO.toString())
                .clickShowResults()
                .atPage();

       searchPage.getBrandResults().forEach(x-> Assertions.assertEquals(BRAND, x));
       searchPage.getYearResults().forEach(x->Assertions.assertTrue(YEAR_FROM <= x && x <= YEAR_TO));
       searchPage.getPriceResults().forEach(x->Assertions.assertTrue(PRICE_FROM <= x && x <= PRICE_TO));
    }
}

